## Public Bank by la Capsule


### Installation

```shell
yarn install
```

#### Si vous avez une puce Mac M1, vous devrez peut-être lancer avec  `PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true`.

```shell
PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true yarn install
```

### Lancer l'app dans un premier terminal

```shell
yarn dev
```


### Démarrer Cypress dans un second terminal

```shell
yarn cypress:open
```
